<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Création d'un client</title>
<link type="text/css" rel="stylesheet" href="formulaire.css">
</head>
<body>
<c:import url="menu.jsp"></c:import>
<br>
<form method ="post" action="creationClient">
<fieldset class="scheduler-border">
<legend class="scheduler-border">inscrivez-vous</legend>
	<div><p class="titre">Formulaire d'ajout d'un client</p></div>
	<div>
		<label for="nom"> Nom * :</label>
		<input type="text" id="nom" name="nom" value="${nomSaisi}"/>
		${nomIncorrect}	
		<span class="erreur">${formC.erreurs['nom']}</span>
	</div>
	<div>
		<label for="prenom"> Prénom * :</label>
		<input type="text" id="prenom" name="prenom" value="${prenomSaisi}"/>
		${prenomIncorrect}
		<span class="erreur">${formC.erreurs['prenom']}</span>
	</div>
	<div>
		<label for="tel"> num de téléphone :</label>
		<input type="text" id="tel" name="tel" value="${telSaisi}"/>${telIncorrect}
		<span class="erreur">${formC.erreurs['tel']}</span>
	</div>
	<div>
		<label for="email"> @mail :</label>
		<input type="text" id="email" name="email" value="${emailSaisi}"/>${emailIncorrect}
		<span class="erreur">${formC.erreurs['email']}</span>
	</div>
	<p class="bouton"> <input type="submit"  value="Ajouter"/> </p>
	</fieldset>
</form>
</body>
</html>