package org.eclipse.dao;

import org.eclipse.model.Client;

public interface ClientDao {
	
	Client save(Client client);

}
