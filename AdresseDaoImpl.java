package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.connection.MyConnection;
import org.eclipse.dao.AdresseDao;
import org.eclipse.model.Adresse;

public class AdresseDaoImpl implements AdresseDao {

	@Override
	public Adresse save(Adresse adresse) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
		try {
		PreparedStatement ps = c.prepareStatement("insert into adresse (rue, codePostal,ville) values (?,?,?); ", PreparedStatement.
		RETURN_GENERATED_KEYS);
		ps.setString(1, adresse.getRue());
		ps.setString(2, adresse.getCodePostal());
		ps.setString(3, adresse.getVille());
		ps.executeUpdate();
		ResultSet resultat = ps.getGeneratedKeys();
		if (resultat.next()) {
		adresse.setId(resultat.getInt(1));
		System.out.println("insertion OK");
		return adresse;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		}
		}
		return null;
		}

}
