package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.connection.MyConnection;
import org.eclipse.dao.ClientDao;
import org.eclipse.model.Client;

public class ClientDaoImpl implements ClientDao {

	@Override
	public Client save(Client client) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
		try {
		PreparedStatement ps = c.prepareStatement("insert into client (nom,prenom,tel,email) values (?,?,?,?); ", PreparedStatement.
		RETURN_GENERATED_KEYS);
		ps.setString(1, client.getNom());
		ps.setString(2, client.getPrenom());
		ps.setString(3, client.getTel());
		ps.setString(4, client.getEmail());
		ps.executeUpdate();
		ResultSet resultat = ps.getGeneratedKeys();
		if (resultat.next()) {
		client.setId(resultat.getInt(1));
		System.out.println("insertion OK");
		return client;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		}
		}
		return null;
		}

	
}
