package org.eclipse.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.eclipse.form.AjoutClientForm;
import org.eclipse.model.Client;
import org.eclipse.service.ClientDaoImpl;

/**
 * Servlet implementation class CreationClientServlet
 */
@WebServlet("/creationClient")
public class CreationClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CREATION_CLIENT = "/WEB-INF/creationClient.jsp";
	public static final String AFFICHER_CLIENT = "/WEB-INF/afficherClient.jsp";
	public static final String ATT_CLIENT = "client";
	public static final String ATT_FORMC = "formC";
	public static final String VUE_SUCCES = "/WEB-INF/afficherClient.jsp";
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_TEL = "tel";
	private static final String CHAMP_EMAIL = "email";
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(CREATION_CLIENT).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		AjoutClientForm formC = new AjoutClientForm();
		Client client = formC.creerPersonne(request);
		request.setAttribute(ATT_CLIENT, client);
		request.setAttribute(ATT_FORMC, formC);
		session.setAttribute("client", client);
//		String message = null;
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");
//		String tel = request.getParameter("tel");
//		String email = request.getParameter("email");

		

		if (formC.getErreurs().isEmpty()) {
			ClientDaoImpl caop = new ClientDaoImpl();
			caop.save(client);
			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
		} else {
			/* Sinon, ré-affichage du formulaire de création avec les erreurs */
			this.getServletContext().getRequestDispatcher(CREATION_CLIENT).forward(request, response);
		}	
		}
//			try {
//				String nom = request.getParameter("nom");
//				String prenom = request.getParameter("prenom");
//				String tel = request.getParameter("tel");
//				String email = request.getParameter("email");
//				Client c = new Client();
//				c.setNom(nom);
//				c.setPrenom(prenom);
//				c.setTel(tel);
//				c.setEmail(email);
//				ClientDaoImpl cdao = new ClientDaoImpl();
//				Client insertedClient = cdao.save(client);
//			} catch (Exception e) {
//
//				e.printStackTrace();
//				this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
//
//			}
//		} else {
//			/* Sinon, ré-affichage du formulaire de création avec les erreurs */
//			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
//		}
	}
//		private void validationNom(String nom) throws Exception {
//			if (nom != null) {
//				if (nom.length() < 2) {
//					throw new Exception("Le nom d'utilisateur doit contenir au moins 2 caractères.");
//				}
//			} else {
//				throw new Exception("Merci d'entrer un nom d'utilisateur.");
//			}
//		}
//
//		private void validationPrenom(String prenom) throws Exception {
//			if (prenom != null) {
//				if (prenom.length() < 2) {
//					throw new Exception("Le nom d'utilisateur doit contenir au moins 2 caractères.");
//				}
//			} else {
//				throw new Exception("Merci d'entrer un nom d'utilisateur.");
//			}
//		}
//		
//		private void validationTel(String tel) throws Exception {
//			if (tel!= null) {
//				if (tel.length() < 4) {
//					throw new Exception("un numéro de tph doit au moins contenir au moins 4 caractères.");
//				}
//			} else {
//				throw new Exception("Merci d'entrer un numéro de téléphone.");
//			}
//		}
//		
//		private void validationMail(String email) throws Exception {
//			if (email!= null) {
//				if (email.length() < 2) {
//					throw new Exception("L'adresse mail doit ^étre valide.");
//				}
//			} else {
//				throw new Exception("Merci d'entrer une adresse mail valide.");
//			}
//		}
//		private void setErreur(String champ, String message) {
//			erreurs.put(champ, message);
//		}
//		
//		public void verifChaine(String s) throws Exception {
//			if (s == null || s.length() < 2)
//				throw new Exception("La chaîne doit comporter au moins deux caractères");
//			char c = s.charAt(0);
//			if (!(c >= 'A' && c <= 'Z'))
//				throw new Exception("La chaîne doit ccommencer par une lettre majuscule");
//			for (int i = 0; i < s.length(); i++) {
//				c = s.charAt(i);
//				if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z'))
//					throw new Exception("La chaîne ne peut contenir que des lettres");
//			}
//
//		}

