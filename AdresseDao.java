package org.eclipse.dao;

import org.eclipse.model.Adresse;

public interface AdresseDao {

	Adresse save(Adresse adresse);
}
