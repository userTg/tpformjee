package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.form.AjoutAdresseForm;

import org.eclipse.model.Adresse;
import org.eclipse.model.Client;
import org.eclipse.service.AdresseDaoImpl;

/**
 * Servlet implementation class CreationAdresseServlet
 */
@WebServlet("/creationAdresse")
public class CreationAdresseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CREATION_ADRESSE = "/WEB-INF/creationAdresse.jsp";
	public static final String ATT_ADRESSE = "adresse";
	public static final String ATT_FORMAD = "formA";
	public static final String VUE_SUCCES_AD = "/WEB-INF/afficherAdresse.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(CREATION_ADRESSE).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		AjoutAdresseForm formA = new AjoutAdresseForm();
		Adresse adresse = formA.creerAdresse(request);
		Client monClient = (Client) session.getAttribute("client");
		request.setAttribute(ATT_ADRESSE, adresse);
		request.setAttribute(ATT_FORMAD, formA);
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
//		String nomClient = monClient.getNom();
//		String pNomClient = monClient.getPrenom();
//		String emailClient = monClient.getEmail();
//		String telClient = monClient.getTel();
		adresse.setRue(rue);
		adresse.setCodePostal(codePostal);
		adresse.setVille(ville);
		adresse.setClient(monClient);
		adresse.getClient().setNom(monClient.getNom());
		adresse.getClient().setPrenom(monClient.getPrenom());
		adresse.getClient().setTel(monClient.getTel());
		adresse.getClient().setEmail(monClient.getEmail());
		
		
		if (formA.getErreurs().isEmpty()) {
		AdresseDaoImpl adao = new AdresseDaoImpl();
		adao.save(adresse);
		this.getServletContext().getRequestDispatcher(VUE_SUCCES_AD).forward(request, response);
		} else {
			/* Sinon, ré-affichage du formulaire de création avec les erreurs */
			this.getServletContext().getRequestDispatcher(CREATION_ADRESSE).forward(request, response);
		}	
//		String message = null;
//	    
//	    Client monClient = (Client) session.getAttribute("client");
//		Adresse adresse = new Adresse();
//		String rue = request.getParameter("rue");
//		String codePostal = request.getParameter("codePostal");
//		String ville = request.getParameter("ville");
//		
//		adresse.setRue(rue);
//		adresse.setCodePostal(codePostal);
//		adresse.setVille(ville);
//		adresse.setClient(monClient);
//		adresse.getClient().setNom(monClient.getNom());
//		adresse.getClient().setPrenom(monClient.getPrenom());
//		adresse.getClient().setTel(monClient.getTel());
//		adresse.getClient().setEmail(monClient.getEmail());
//		
//		if (monClient.getNom().trim().isEmpty() || monClient.getPrenom().trim().isEmpty() || monClient.getTel().trim().isEmpty() 
//				|| monClient.getEmail().isEmpty() || rue.isEmpty() || codePostal.isEmpty() || ville.isEmpty()) {
//			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creationClient\">Cliquez ici</a> pour accéder au formulaire de création d'une commande.";
//		} else 
//		{
//			 message = "Adresse créée avec succès !";
//			 AdresseDaoImpl adao = new AdresseDaoImpl();
//			 adao.save(adresse);
//		}
//		
//		request.setAttribute("message", message);
//		request.setAttribute("adresse", adresse);
//		
//		this.getServletContext().getRequestDispatcher(VUE_SUCCES_AD).forward(request, response);
	
//		AjoutAdresseForm formAd = new AjoutAdresseForm();
//		String message = null;
//		Adresse adresse = formAd.creerAdresse(request);
//		Client monClient = (Client) session.getAttribute("client");
//		session.setAttribute(ATT_ADRESSE, adresse);
//		request.setAttribute("message", message);
//
//		if (formAd.getErreurs().isEmpty()) {
//			String rue = request.getParameter("rue");
//			String codePostal = request.getParameter("codePostal");
//			String ville = request.getParameter("ville");
//			Adresse a = new Adresse();
//			a.setRue(rue);
//			a.setCodePostal(codePostal);
//			a.setVille(ville);
//			a.setClient(monClient);
//			AdresseDaoImpl adao = new AdresseDaoImpl();
//			Adresse insertedAdresse = adao.save(a);
//
//			this.getServletContext().getRequestDispatcher(VUE_SUCCES_AD).forward(request, response);
//		} else {
//			/* Sinon, ré-affichage du formulaire de création avec les erreurs */
//			this.getServletContext().getRequestDispatcher(VUE_SUCCES_AD).forward(request, response);
//		}
	}

}
