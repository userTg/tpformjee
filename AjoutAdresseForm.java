package org.eclipse.form;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.eclipse.model.Adresse;

public class AjoutAdresseForm {
	
	private static final String CHAMP_RUE = "rue";
	private static final String CHAMP_VILLE ="ville";
	private static final String CHAMP_CODEPOSTAL = "codePostal";

	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}
	
	public Adresse creerAdresse(HttpServletRequest request) {
		String rue = getValeurChamp(request, CHAMP_RUE);
		String codePostal = getValeurChamp(request, CHAMP_CODEPOSTAL);
		String ville = getValeurChamp(request, CHAMP_VILLE);
		Adresse adresse = new Adresse();
		
		try {
			validationRue(rue);
		} catch (Exception e) {
			setErreur(CHAMP_RUE, e.getMessage());
		}
		
		adresse.setRue(rue);
		
		try {
			validationCodePostal(codePostal);
		} catch (Exception e) {
			setErreur(CHAMP_CODEPOSTAL, e.getMessage());
		}
	
		adresse.setCodePostal(codePostal);
		
		try {
			validationVille(ville);
		} catch (Exception e) {
			setErreur(CHAMP_VILLE, e.getMessage());
		}
		
		adresse.setVille(ville);
		
		
		if (erreurs.isEmpty()) {
			resultat = "Succès de la création du client.";
		} else {
			resultat = "Échec de la création du client.";
		}
		return adresse;
	}
	
	private void validationRue(String rue) throws Exception {
		if (rue!= null) {
			if (rue.length() < 2) {
				throw new Exception("Un nom de rue doit comporter au moins 2 cactères.");
			}
		} else {
			throw new Exception("Merci d'entrer une rue valide.");
		}
	}
	
	private void validationVille(String ville) throws Exception {
		if (ville!= null) {
			if (ville.length() < 2) {
				throw new Exception("Une ville doit comporter au moins 2 cactères.");
			}
		} else {
			throw new Exception("Merci d'entrer une ville valide.");
		}
	}
	
	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}
	
	private void validationCodePostal(String tel) throws Exception {
		if (tel!= null) {
			if (tel.length() < 4) {
				throw new Exception("un code postal doit au moins contenir au moins 4 caractères.");
			}
		} else {
			throw new Exception("Merci d'entrer un code postal valide.");
		}
	}
	
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}
	
	public void verifChaine(String s) throws Exception {
		if (s == null || s.length() < 2)
			throw new Exception("La chaîne doit comporter au moins deux caractères");
		char c = s.charAt(0);
		if (!(c >= 'A' && c <= 'Z'))
			throw new Exception("La chaîne doit ccommencer par une lettre majuscule");
		for (int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z'))
				throw new Exception("La chaîne ne peut contenir que des lettres");
		}

	}
}
